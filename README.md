## 母婴24

## Build Setup

``` bash
# install dependencies
npm install

# 测试环境编译
npm run test

# 正式环境编译
npm run build

# 其他
npm run dev
```
## 技术栈
mpvue
vuex
vue
vue-router


## 项目结构：

api --- 前端接口路径配置

assets --- 前端 公共样式

components --- 前端 组件库

contants --- 页面中默认图片地址

mixins --- 公共页面代码(混入)

pages --- 页面

utils/tool --- 工具包

store --- 前端 vue状态管理（vuex)

router --- 前端 路由管理

static --- 图片资源

## 路径详情
/pages/Order/list --- 订单列表页面

/pages/Order/detail ---- 订单详情页面

/pages/Order/confirm --- 订单确认页面

/pages/Order/resutl --- 订单成功页面

/pages/Goods/list --- 商品列表页面

/pages/Goods/searchList --- 商品搜索页面

/pages/Goods/detail --- 正常商品详情页面

/pages/Goods/activitysDetail --- 活动商品详情页面

/pages/Address/manageAddress --- 地址列表页面

/pages/Address/addAddress --- 添加地址页面

/pages/Address/editAddress --- 编辑地址页面

/pages/Address/changeStore --- 切换门店

/pages/Activitys/newManner --- 新人礼

/pages/Activitys/redPacketDetails --- 领取红包页面

/pages/Auth/start --- 项目启动页面

/pages/Auth/login --- 项目授权页面

/pages/loading/index --- 物料码登录页面

/pages/Common/helpCenter --- 帮助中心

/pages/Common/logistics --- 物流详情页面

/pages/Home/index --- 首页

/pages/Carts/index --- 购物车页面

## 注意事项
代码编写注意
1.模板使用函数不支持，复杂表达式也不支持，只支持computed，因此如果数据复杂，要先做好处理
2.img 标签不支持引用本地图片等资源，可以用使用小程序image标签或者引用服务器上的图片
3.所有页面里面的created生命周期函数 都会在小程序加载的时候， 一次性执行，而不是每进入一个页面执行一次，用mounted或者onLoad或者onReady代替
4.渲染部分会转成 wxml ，wxml 不支持过滤器，所以这部分功能不支持。
5.小程序样式使用rpx代替px,可以根据屏幕宽度进行自适应,在 iPhone6上，1rpx = 0.5px = 1物理像素。
6.eslint工具：使用全等===
For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
