export const showLoading = (text = '') => { // 显示加载
  wx.showLoading({
    title: String(text),
    mask: true
  })
}
export const hideLoading = () => { // 隐藏加载
  wx.hideLoading()
}
export const toast = (text = '') => { // 提示
  wx.showToast({
    title: String(text),
    icon: 'none',
    duration: 3000
  })
}
export const alert = (c, o) => { // 弹框
  let obj = {
    content: String(c),
    showCancel: false
  }
  if (o) for (let i in o) obj[i] = o[i]
  wx.showModal(obj)
}
export const setUrlObj = (obj) => { // 对象转url传参
  let str = []
  for (let i in obj) {
    str.push(i + '=' + setFilterMark(obj[i]))
  }
  return str.join('&')
}
export const getUrlObj = (str) => { // url参数转 对象
  const arr = str.split('=')
  let obj = {}
  arr.map(item => {
    const data = item.split('=')
    obj[data[0]] = getFilterMark(data[1])
  })
  return obj
}
export const setFilterMark = (obj) => { // 过滤url 特殊符号      &=1 ?=2
  return (JSON.stringify(obj)).replace(new RegExp(/(&)/g), 'mark-1').replace(new RegExp(/(\?)/g), 'mark-2')
}
export const getFilterMark = (obj) => { // 特殊符号 转回来
  return obj.replace(new RegExp(/(mark-1)/g), '&').replace(new RegExp(/(mark-2)/g), '?')
}
export const deepCopy = (p, c) => { // 对象拷贝
  c = (c || {})
  for (var i in p) {
    if (p[i] && typeof p[i] === 'object') {
      c[i] = (p[i].constructor === Array) ? [] : {}
      deepCopy(p[i], c[i])
    } else {
      c[i] = p[i]
    }
  }
  return c
}
