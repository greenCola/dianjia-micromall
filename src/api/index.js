import fly from './config'
import flyhost from './config_host'
export default {
  // effective: (param) => fly.get(`/api/redPackActivity/effective`,param),
  // trigger: (param) => fly.post(`/api/redPackActivity/${param.id}/trigger?customerId=${param.customerId}&storeId=${param.storeId}`),
  getStoreList: (param) => fly.get(`api/store/byPosition`, param),
  Public: { // 公共
    getOpenId: (param) => fly.post(`api/customers/auth?code=${param.code}&storeId=${param.storeId}`), // 获取openid 已注册的用户信息
    getUserPhone: (param) => fly.get(`api/customers/phone`, param), // 获取用户手机号
    userLogin: (param) => fly.post('api/customers/register', param), // 用户注册
    getShopInfo: (param) => fly.get(`api/stores/${param.storeId}`), // 通过storeId获取门店信息
    getWxQrCode: (param) => fly.get(`api/wxQrCode/generateQrcode`, param), // 获取小程序二维码
    getStoreInfoByStoreCode: (params) => fly.get('/api/stores/byStoreCode', params), // 跳转loading
    getPayConfig: () => fly.get(`/api/sysConfig/getPayConfig`), // 微信绑定 支付
    sendWxMessage: (param) => fly.post(`/api/order/sendWxMessage`, param) // 微信模版消息的发送---支付成功
  },
  Index: { // 首页 爆款预留
    getClassifyList: (param) => fly.get(`/api/storeSkus/V2/home`, param),
    // getClassifyList: (param) => fly.get(`/api/hotSaleProducts/home`, param),
    getBurstList: () => fly.get(`/api/activities/hotDaily/home`),
    storeSale: () => fly.get('api/hotSale') // 门店现货
  },
  Carts: { // 购物车
    getCarTotalCount: (param) => fly.get(`/api/shopCarts/my/totalCount?storeId=${param.storeId}`), // 商品总数量
    getShopCarList: (param) => fly.get(`/api/shopCarts/my`, param), // 购物车商品列表
    postShopCarts: (body) => fly.post('/api/shopCarts', body), // 添加商品到购物车
    getShopCarDelete: (param) => fly.delete(`/api/shopCarts/${param.id}`), // 删除单个商品
    getShopCarPut: (param) => fly.put(`/api/shopCarts/${param.id}/count?count=${param.count}`), // 更新单个商品数量
    submitSelectShopInfo: (param) => fly.get(`/api/shopCarts/my/confirm?ids=${param.ids}&storeId=${param.storeId}`) // 结算
  },
  my: { // 我的
    getCustomerAddressList: (param) => fly.get(`/api/customerAddress/my`, param), // 收货地址管理
    addCustomerAddressList: (param) => fly.post(`/api/customerAddress`, param), // 新增
    updataCustomerAddressList: (param) => fly.put(`/api/customerAddress`, param), // 更新
    delCustomerAddressList: (param) => fly.delete(`/api/customerAddress/${param.id}`), // 删除
    getCustomerAddressByAddressId: (param) => fly.get(`/api/customerAddress/${param.id}`), // 通过id获取地址详情
    getStoreListByLocation: (param) => fly.get(`/api/stores/byPosition`, param), // 通过地理坐标获取门店列表
    setDefaultStore: (param) => fly.put(`/api/customers/setDefaultStore?storeId=${param.storeId}`), // 切换门店后设置默认门店
    getCostomerDeposits: (param) => fly.get(`/api/microMallCustomerDeposits/my`, param), // 我的页面
    getCustomerAddresDefault: () => fly.get(`/api/customerAddress/my/default`) // 获取我的默认地址
  },
  shopDetail: { // 详情页面
    // getShopDetail: (param) => fly.get(`/api/storeSkus/${param.id}?storeId=${param.storeId}`),
    getShopDetail: (param) => fly.get(`/api/storeSkus/V2/${param.id}`), // 页面商品详情
    getMyCustomerDeposits: () => fly.get(`/api/microMallCustomerDeposits/my`),
    getMyOrderList: (param) => fly.get(`/api/microMallCustomerDeposits/my`, param), // 获取我的订单列表
    // postSaveOrder: (param) => fly.post(`/api/order/saveOrder`, param), // 订单支付
    postSaveOrder: (param) => fly.post(`/api/order/v2`, param), // 订单支付提交页面重构接口
    postActivitySaveOrder: (param) => fly.post(`/api/activities/v2/${param.id}/buy?buyCount=${param.buyCount}`, param), // 活动商品订单支付页面
    postCheckStoreSkus: (param) => fly.post(`/api/storeSkus/V2/check`, param) // 正常商品的判断库存的概念
    // getStockCount: (param) => fly.get(`/api/storeSkus/checkStock`, param) // 获取订单库存 --- 废弃
  },
  Classify: { // 分类
    getFirstLevel: (param) => fly.get(`/api/Categories/firstLevel`), // 获取所有一级分类
    getSecendLevel: (param) => fly.get(`/api/Categories/secondLevel/${param.categoryId}`), // 根据一级分类id获取所有二级分类
    getAllClassify: (param) => fly.get(`/api/Categories/getAll`), // 获取全部分类
    getHotBrand: (param) => fly.get(`/api/Categories/getHotBrand`, param), // 获取热门品牌
    getHotBrandProducts: (param) => fly.get(`/api/hotSaleProducts/byCategoryId?storeId=${param.storeId}&categoryId=${param.categoryId}`) // 获取热门品牌的商品
  },
  RedPacket: {
    effective: (param) => fly.get(`/api/redPackActivities/effective`, param), // 获取红包活动
    trigger: (param) => fly.post(`/api/redPackActivities/${param.id}/trigger?storeId=${param.storeId}`), // 领取红包
    triggerOrder: (param) => fly.post(`/api/redPackActivities/${param.id}/trigger?orderSrl=${param.orderSrl}&storeId=${param.storeId}`) // 领取红包
  },
  Search: {
    searchGoods: (param) => fly.get(`/api/storeSkuSearches?sort=imgFlag,desc&sort=id,desc`, param) // 搜索
  },
  Order: {
    getOrderTabCount: (param) => fly.get(``), // 获取订单列表 tab显示的数量
    getOrderListByTab: (param) => fly.get(`/api/order/my/getOrderListByStatus?status=${param.status}&page=${param.page}&size=${param.size}&storeId=${param.storeId}`), // 用于已支付
    getOrderListNoPay: (param) => fly.get(`/api/order/my/noPayOrderList?storeId=${param}`), // 获取未支付订单
    getOrderById: (param) => fly.get(`/api/order/my/getOrderById/${param.id}`),
    getExpressInfo: (param) => fly.get(`/api/expressInfos/byJoinId?joinId=${param.joinId}`),
    cancelOrder: (param) => fly.get(`/api/order/cancelOrder?totalSrl=${param.totalSrl}`), // 取消订单
    payOrder: (param) => fly.get(`/api/order/my/payOrder`, param), // 支付订单
    payGroupBuyOrder: (param) => fly.get(`/api/order/payGroupBuyOrder`, param), // 拼团支付订单
    addShopFromOrder: (param) => fly.post(`/api/shopCarts/fromOrder?orderId=${param.orderId}&storeId=${param.storeId}`), // 根据订单添加商品到购物车
    getNoPayNum: (param) => fly.get(`/api/order/my/noPayNum?storeId=${param}`),
    confirm: (param) => fly.get(`/api/order/${param.id}/confirm?formId=${param.formId}`),
    getOrderNum: (param) => fly.get(`/api/order/countByStatus?storeId=${param}`),
    getOrderStoreSkuids: (param) => fly.get(`/api/activities/detailByStoreSkuIds?storeSkuIds=${param}`) // 改接口不用
  },
  newManner: {
    geBanners: () => fly.get(`/api/banners/byType?type=NEW_CUSTOMER`), // 首页新人有礼图片
    //  getDetailByStoreSkuId: (param) => fly.get(`/api/activities/detailByStoreSkuId?storeSkuId=${param.storeSkuId}`), // 获取商品详情页面中活动数据
    // getList: () => fly.get(`/api/activities/newCustomer`), // 获取新人有礼的商品列表
    getActivityList: (param) => fly.get(`/api/activities/v2/byType?activityType=${param.activityType}`), // 获取活动的列表
    checkActivities: (param) => fly.get(`/api/activities/v2/${param.id}/check?buyCount=${param.buyCount}`), // 判断活动商品还能不能再次购买
    // getActivitiesDetail: (param) => fly.get(`/api/activities/v2/${param.id}/detail`), // 活动该活动商品的详情
    getActivitiesDetail: (param) => fly.get(`/api/activities/v2/detail/byStoreSkuId?storeSkuId=${param.id}`), // 活动该活动商品的详情
    getRecommendStoreSkus: () => fly.get(`/api/recommendStoreSkus/byType?type=NEW_CUSTOMER`) // 人气推荐
  },
  // 团购
  GroupBuy: {
    getGroupBuyDetails: ({actGroupId}) => fly.get(`/api/actGroups/detail/${actGroupId}`), // 拼团详情
    getGroupBuyMyList: (param) => fly.get(`/api/actGroups/myList`, param), // 我的拼团列表
    getGroupBuyList: (param) => fly.get(`/api/actGroupBuy/list `, param), // 拼团活动列表 ON_LINE("上线"),OUT_LINE("下线"); sort: 'soldCount,desc'
    getProductDetail: (param) => fly.get(`/api/actGroupBuy/${param.id}?storeId=${param.storeId}`), // 团购商品详情
    getGroupingData: (param) => fly.get(`/api/product/productDetail/${param.productId}`), // 获取待成团数据列表
    gethasGroupedData: (param) => fly.get(`/api/product/productDetail/${param.productId}`), // 获已经待成团数据列表
    postActGroups: (param) => fly.post(`/api/actGroups`, param), // 开团支付接口
    postActGroupsJoin: (param) => fly.post(`/api/actGroups/join`, param), // 拼团支付的接口
    searchByOrderId: (param) => fly.get(`/api/actGroups/detail/byOrderId?orderId=${param.orderId}`) // 开团成功之后
  },
  // 礼物
  Gift: {
    getGiftSendDetail: ({giftId}) => fly.get(`/api/actGiftOrders/${giftId}`), // 礼物送出礼物详情
    getGiftReceiveDetail: ({giftId}) => fly.get(`/api/actGiftOrderReceives/${giftId}`), // 礼物收到礼物详情
    getGiftDetail: (param) => fly.get(`/api/actGift/${param.id}?storeId=${param.storeId}`), // 礼物商品详情
    getGiftList: (param) => fly.get(`/api/actGift/list`, param), // 礼物列表
    createActGift: (param) => fly.post(`/api/actGiftOrders/create`, param), // 生成礼物
    getSendGiftList: (param) => fly.get(`/api/actGiftOrders/myList`, param), // 我送出的礼物列表
    getReceivedGiftList: (param) => fly.get(`/api/actGiftOrderReceives/myList`, param), // 我收到的礼物列表
    postReceivedGift: (param) => fly.post(`/api/actGiftOrderReceives`, param), // 接受礼物
    getPayInfo: (param) => fly.get(`/api/payinfo/getPayInfo/${param.srl}`, param), // 获取支付信息
    grabGift: ({actGiftOrderId}) => fly.post(`/api/actGiftOrderReceives/grabGift?actGiftOrderId=${actGiftOrderId}`), // 抢礼物
    giftCreateOrder: (param) => fly.put(`/api/actGiftOrderReceives/${param.id}/createOrder`, param.orderBaseInfo) // 礼物下单
  },
  // 门店搜索
  Store: {
    getStoreSearch: (param) => fly.get(`/api/stores/search`, param) // 搜索接口
  },
  // 首页banenr
  TopBanner: {
    getTopBanner: () => fly.get(`/api/topBanner/bannerList`)
  },
  Notice: {
    getNotice: (param) => fly.get(`/api/actGroupDescription/getByUserId`, param)
  },
  // 秒杀
  SecKill: {
    getSecKillHome: (param) => fly.get(`/api/ActFlashSales/home`, param), // 首页
    getSecKillList: (param) => fly.get(`/api/ActFlashSales`, param), // 秒杀列表
    getSecKillDetail: (param) => fly.get(`/api/ActFlashSales/${param.id}?storeId=${param.storeId}`), // 秒杀详情
    postSecKillSaleCustomer: (param) => fly.post(`/api/ActFlashSales/${param.id}/actFlashSaleCustomer?buyCount=${param.buyCount}&storeId=${param.storeId}`), // 获取秒杀资格
    postSecKillOrder: (param) => fly.post(`/api/ActFlashSaleOrders`, param), // 下订单
    getGroupDescription: (param) => fly.get(`/api/actGroupDescription/getByUserId?userId=${param.useId}`), // 门店说明
    getMySecLillList: (param) => fly.get(`/api/ActFlashSaleOrders/my`, param), // 我的秒杀
    postSubscribes: (param) => fly.post(`/api/ActFlashSaleSubscribes?actFlashSaleId=${param.actFlashSaleId}&formId=${param.formId}`), // 添加预约
    updateSubscribes: (param) => fly.put(`/api/ActFlashSaleSubscribes`, param) // 取消预约
  },
  // 线下支付
  Pay: {
    payOffine: (param) => flyhost.post(`/api/pay/preCreate`, param)
  },
  // 优惠券
  Coupon: {
    postActCustomerCoupon: (param) => fly.post(`/api/ActCustomerCoupons`, param), // 领取优惠券
    getActCustomerCoupon: (param) => fly.get(`/api/ActCustomerCoupons/my/byStatus`, param) // 根据状态获取优惠券
  }
}
