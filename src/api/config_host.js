import RouterPath from '@router/config'
const Fly = require('flyio/dist/npm/wx')
const flyhost = new Fly()

switch ((process.TYPE_ENV)) {
  case 'production':
    flyhost.config.baseURL = 'https://dpcn.dianjia001.com'
    break
  case 'testing':
    flyhost.config.baseURL = 'http://pay.17dianjia.net'
    break
  default:
    flyhost.config.baseURL = 'http://pay.17dianjia.net'
    break
}

flyhost.interceptors.request.use(request => {
  const token = wx.getStorageSync('token')
  if (token) {
    request.headers['Authorization'] = 'Bearer ' + token
  }
  return request
})
flyhost.interceptors.response.use(
  response => {
    return response
  },
  err => {
    handleResult(err)
    console.warn('接口请求失败', err)
    return err
  }
)
const handleResult = res => {
  let code = res.status
  switch (code) {
    case 400 :
      wx.reLaunch({
        url: RouterPath.Home
      })
      break
    case 500 :
      if (res.response.data) {
        // 团购支付不弹框提示
        res.response.data.path !== '/api/order/payGroupBuyOrder' && showToast(res.response.data.message)
      }
      break
    case 401:
      // wx.navigateTo({
      //   url: '/pages/impower/index?fromPage=index'
      // })
      break
    case 403:
      break
  }
}
const showToast = (message = '') => {
  wx.showToast({
    title: message,
    icon: 'none',
    duration: 3000
  })
}
export default flyhost
