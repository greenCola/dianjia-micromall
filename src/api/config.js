import RouterPath from '@router/config'
const Fly = require('flyio/dist/npm/wx')
const fly = new Fly()

switch ((process.TYPE_ENV)) {
  case 'production':
    fly.config.baseURL = 'https://dpcn.dianjia001.com'
    break
  case 'testing':
    fly.config.baseURL = 'https://dpcn.17dianjia.net'
    break
  default:
    // fly.config.baseURL = 'https://dpcn.dianjia001.com'
    fly.config.baseURL = 'https://dpcn.17dianjia.net'
    // fly.config.baseURL = 'http://10.23.0.173:8081/'
    break
}
// if (process.env.NODE_ENV === 'production') {
//   // fly.config.baseURL = 'http://47.93.178.138:8083/'
//   // test
//   fly.config.baseURL = 'https://dpcn.17dianjia.net'
//   // online
//   // fly.config.baseURL = 'https://dpcn.dianjia001.com'
// } else {
//   // fly.config.baseURL = 'http://47.93.178.138:8083/'
//   fly.config.baseURL = 'https://dpcn.17dianjia.net'
//   // fly.config.baseURL = 'https://dpcn.dianjia001.com'
// }
fly.interceptors.request.use(request => {
  const token = wx.getStorageSync('token')
  if (token) {
    request.headers['Authorization'] = 'Bearer ' + token
  }
  return request
})
fly.interceptors.response.use(
  response => {
    return response
  },
  err => {
    handleResult(err)
    console.warn('接口请求失败', err)
    return err
  }
)
const handleResult = res => {
  let code = res.status
  switch (code) {
    case 400 :
      wx.reLaunch({
        url: RouterPath.Home
      })
      break
    case 500 :
      if (res.response.data) {
        // 团购支付不弹框提示
        res.response.data.path !== '/api/order/payGroupBuyOrder' && showToast(res.response.data.message)
      }
      break
    case 401:
      // wx.navigateTo({
      //   url: '/pages/impower/index?fromPage=index'
      // })
      break
    case 403:
      break
  }
}
const showToast = (message = '') => {
  wx.showToast({
    title: message,
    icon: 'none',
    duration: 3000
  })
}
export default fly
