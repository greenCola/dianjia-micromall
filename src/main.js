import Vue from 'vue'
import App from './App.vue'
import store from './store/index'
import './assets/weui/weui.css'
import './assets/main.styl'
import './utils/tdweapp'
import Utils from '@utils/utils'
import MpvueRouterPatch from 'mpvue-router-patch'
Vue.use(MpvueRouterPatch)
Vue.config.productionTip = false
App.mpType = 'app'
// 挂载store到vue实例上
if (process.env.NODE_ENV === 'production') {
  Vue.prototype.$storageServer = 'https://dj001.oss-cn-beijing.aliyuncs.com/'
} else {
  Vue.prototype.$storageServer = 'https://dj001.oss-cn-beijing.aliyuncs.com/'
}

Vue.prototype.$store = store
Vue.prototype.$Utils = Utils
const app = new Vue(App)
app.$mount()

export default {
  // 这个字段走 app.json
  config: {
    window: { // 小程序所有页面的顶部背景颜色，文字颜色定义
      // backgroundTextStyle: 'dark',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: '',
      navigationBarTextStyle: 'black'
      // enablePullDownRefresh: true
    },
    'networkTimeout': { // 设置网络请求的超时时间
      'request': 10000,
      'downloadFile': 10000
    },
    'permission': {
      'scope.userLocation': {
        'desc': '我们将为您展示附近的母婴门店'
      }
    },
    debug: false
  }
}
