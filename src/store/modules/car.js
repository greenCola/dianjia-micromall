import * as types from '../types'
import API from '../../api/index'
const states = {
  shopList: [],
  shopCarCount: null,
  allSelected: false
}
const getters = {
  shopList: state => state.shopList,
  shopCarCount: state => state.shopCarCount,
  allSelected: state => state.allSelected
}

const actions = {
  [types.DO_GET_SHOP_CAR] ({
    commit, state }, query) {
    return API.Carts.getShopCarList(query).then(res => {
      commit(types.SET_CAR_INFO, res.data)
      return res.data
    })
  },
  [types.DO_GET_SHOPCAR_COUNT] ({ commit, state }, query) {
    return API.Carts.getCarTotalCount(query).then(response => {
      return response.data
    })
  },
  [types.DO_POST_SHOPCARS] ({ commit, state }, query) {
    return API.Carts.postShopCarts(query).then(response => {
      return response.data
    })
  },
  [types.DO_DELETE_PRODUCT] ({ commit, state }, query) {
    return API.Carts.getShopCarDelete(query).then(response => {
    })
  },
  [types.DO_UPDATE_SALE_NUM] ({ commit, state }, query) {
    return API.Carts.getShopCarPut(query).then(response => {
      return response
    })
  },
  [types.DO_SAVE_PRODUCT_CAR] ({ commit, state }, query) {
    return API.Carts.submitSelectShopInfo(query).then(response => {
      if (response) {
        return response.data
      }
    })
  }
}
const mutations = {
  [types.SET_CAR_INFO] (state, data) {
    state.shopList = data
  },
  [types.DO_SET_SHOPCAR_COUNT] (state, data) {
    state.shopCarCount = data
  },
  [types.SET_ALL_SELECTED] (state, data) {
    state.allSelected = data
  }
}
export default {
  states,
  getters,
  mutations,
  actions
}
