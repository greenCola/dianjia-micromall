
const states = {
  SecKillOrderInfo: {}
}
const getters = {
  SecKillOrderInfo: state => state.SecKillOrderInfo
}

const actions = {
  addSecOrderInfo ({ commit }, data) {
    commit('SET_SECKILL_ORDER_INFO', data)
  }
}
const mutations = {
  SET_SECKILL_ORDER_INFO (state, data = {}) {
    state.SecKillOrderInfo = data
  }
}
export default {
  states,
  getters,
  mutations,
  actions
}
