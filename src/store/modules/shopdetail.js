import API from '../../api'
import * as types from '../types'
// import moment from 'moment'

// state
const state = {
  shopDetailData: {},
  queryData: {},
  stockCount: {},
  expressInfo: {},
  setCheckShop: []
}
// getters
const getters = {
  shopDetailData: state => state.shopDetailData,
  queryData: state => state.queryData,
  stockCount: state => state.stockCount,
  expressInfo: state => state.expressInfo,
  setCheckShop: state => state.setCheckShop
}
// actions
const actions = {
  [types.DO_GET_SHOPDETAIL] ({ commit, state }, query) {
    return API.shopDetail.getShopDetail(query).then(response => {
      return response.data
    })
  },
  [types.DO_GET_ADDRESS_DEFAULT] ({ commit, state }) {
    return API.my.getCustomerAddresDefault().then(response => {
      return response.data
    })
  },
  [types.DO_GET_CUSTOMER_DEPOSITS] ({ commit, state }) {
    return API.shopDetail.getMyCustomerDeposits().then(response => {
      return response.data
    })
  },
  [types.DO_POST_SAVE_ORDER] ({ commit, state }, query) {
    return API.shopDetail.postSaveOrder(query).then(response => {
      return response.data
    })
  },
  [types.GET_EXXPRESS_INFO] ({ commit, state }, query) {
    return API.Order.getExpressInfo(query).then(response => {
      console.log(response)
      commit(types.SET_EXXPRESS_INFO, response.data)
      return response.data
    })
  }
}
// mutations
const mutations = {
  [types.SET_QUERY_DATA] (state, data) { // 保存立即购买的信息
    state.queryData = data
  },
  [types.SET_EXXPRESS_INFO] (state, data) { // 保存立即购买的信息
    state.expressInfo = data
  },
  [types.SET_CHECK_SHOP] (state, data) { // 保存立即购买判断门店商品信息
    state.setCheckShop = data
  }
}
export default {
  state,
  getters,
  actions,
  mutations
}
