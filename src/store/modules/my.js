import * as types from '../types'
import API from '../../api/index'
const states = {
  store: {}
}

const getters = {
  store: state => state.store
}

const mutations = {
  [types.GET_STORELIST_BYLOCATION_MUTATION] (state, data) {
    state.storeName = data.storeName
    state.storeId = data.storeId
  }
}

const actions = {
  [types.GET_STORELIST_BYLOCATION_ACTION] ({ commit }, query) {
    return API.getStoreList(query).then(res => {
      commit(types.GET_STORELIST_BYLOCATION_MUTATION, res.data)
      return res.data
    })
  }
}

export default {
  states,
  getters,
  mutations,
  actions
}
