import * as types from '../types'
import API from '../../api/index'
const states = {
  detailAction: {}
}
const getters = {
  detailAction: state => state.detailAction
}

const actions = {
  [types.DO_GET_DETAIL_ACTION] ({ commit, state }, query) {
    return API.newManner.getDetailByStoreSkuId(query).then(response => {
      return response.data
    })
  }
}
const mutations = {
}
export default {
  states,
  getters,
  mutations,
  actions
}
