const states = {
  placeOrderInfo: {}
}
const getters = {
  placeOrderInfo: state => state.placeOrderInfo
}

const actions = {
  addPlaceOrderInfo ({ commit }, data) {
    commit('SET_PLANCE_ORDER_INFO', data)
  }
}
const mutations = {
  SET_PLANCE_ORDER_INFO (state, data = {}) {
    state.placeOrderInfo = data
  }
}
export default {
  states,
  getters,
  mutations,
  actions
}
