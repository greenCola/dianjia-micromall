// Acount
// -- actions
export const DO_GET_SMSCODE = 'DO_GET_SMSCODE'
export const GET_OPEN_ID = 'GET_OPEN_ID'
export const DO_LOGIN_SUBMIT = 'DO_LOGIN_SUBMIT'
export const GET_USER_INFO = 'GET_USER_INFO'
export const GET_PHONE_ACTIONS = 'GET_PHONE_ACTIONS'
export const GET_DOOR_ISUSER = 'GET_DOOR_ISUSER'
// -- mutations
export const DO_LOGIN_SUCCESS = 'DO_LOGIN_SUCCESS'
export const SET_ACCOUNT = 'SET_ACCOUNT'
export const GET_OPENID_SUCCESS = 'GET_OPENID_SUCCESS'
export const SET_HEADER_PARAM = 'SET_HEADER_PARAM'
export const DO_CHECK_OPENIDEXIST = 'DO_CHECK_OPENIDEXIST'
export const DO_LOGIN_SUBMIT_SUCCESS = 'DO_LOGIN_SUBMIT_SUCCESS'
export const SAVE_VID = 'SAVE_VID'
export const SAVE_UID = 'SAVE_UID'
export const SAVE_SID = 'SAVE_SID'
export const SET_SHAREEID = 'SET_SHAREEID'
export const GET_USER_INFO_SUCCESS = 'GET_USER_INFO_SUCCESS'
export const GET_PHONE_SUCCESS = 'GET_PHONE_SUCCESS'
export const SET_USER_MES = 'SET_USER_MES'
export const SET_SESSION_MESAGE = 'SET_SESSION_MESAGE'
export const SET_SESSION_POSTER = 'SET_SESSION_POSTER'
export const LOGIN_OUT = 'LOGIN_OUT' // 登出
// browser
// -- actions
export const DO_REDIRECT = 'DO_REDIRECT'
export const GET_STORE_LIST = 'GET_STORE_LIST'
export const DO_MARK_DEFAULT_STORE = 'DO_MARK_DEFAULT_STORE'
export const DO_CHANGE_STORE_LIST = 'DO_CHANGE_STORE_LIST'
export const DO_CHECK_OPENIDEXIST_SUCCESS = 'DO_CHECK_OPENIDEXIST_SUCCESS'
export const GET_SEARCH_PRODUCTLIST_BY_PCATEGORYID = 'GET_SEARCH_PRODUCTLIST_BY_PCATEGORYID'
// -- mutations
// --actions
export const SET_SHOPLIST = 'SET_SHOPLIST'
export const GET_STORE_LIST_SUCCESS = 'GET_STORE_LIST_SUCCESS'
export const SET_CHECKED_STORE = 'SET_CHECKED_STORE'
export const SET_HOME_REFRESH = 'SET_HOME_REFRESH'
export const SET_FROM_SHARE = 'SET_FROM_SHARE'
export const GET_STORENAME = 'GET_STORENAME'

// --mutations
export const SET_SHOPLIST_DATA = 'SET_SHOPLIST_DATA'
export const DO_GET_SMSCODE_SUCCESS = 'DO_GET_SMSCODE_SUCCESS'
export const CLEAR_SHOPLIST_DATA = 'CLEAR_SHOPLIST_DATA'
export const GET_STORENAME_MUTATONS = 'GET_STORENAME_MUTATONS'
// --actions
export const SHOP_DOOR_DATA = 'SHOP_DOOR_DATA'
export const PROJECTILE_ACTION = 'PROJECTILE_ACTION'
export const CONFIRM_GOODS_DATA = 'CONFIRM_GOODS_DATA'
export const AFTER_SALE_DATA = 'AFTER_SALE_DATA'
export const LOGISTICS_DETAIL_DATA = 'LOGISTICS_DETAIL_DATA'
export const PUSHURL_GETLOGISTICS = 'PUSHURL_GETLOGISTICS'

// --mutaions
export const SHOP_DOOR_DATA_MUTAION = 'SHOP_DOOR_DATA_MUTAION'
export const PROJECTILE_MUTAION = 'PROJECTILE_MUTAION'
export const CONFIRM_GOODS_DATA_MUTAION = 'CONFIRM_GOODS_DATA_MUTAION'
export const AFTER_SALE_DATA_MUTAION = 'AFTER_SALE_DATA_MUTAION'
export const LOGISTICS_DETAIL_DATA_MUTAION = 'LOGISTICS_DETAIL_DATA_MUTAION'
// car
// -- actions
export const DO_SAVE_PRODUCT = 'DO_SAVE_PRODUCT'
export const DO_UPDATE_SALE_NUM = 'DO_UPDATE_SALE_NUM'
export const DO_GET_SHOP_CAR = 'DO_GET_SHOP_CAR'
export const DO_DELETE_PRODUCT = 'DO_DELETE_PRODUCT'
export const DO_SAVE_PRODUCT_CAR = 'DO_SAVE_PRODUCT_CAR'
// --mutations
export const SET_CAR_INFO = 'SET_CAR_INFO'
export const SET_ALL_SELECTED = 'SET_ALL_SELECTED'
export const SAVE_PRODUCT_CAR_SUCCESS = 'SAVE_PRODUCT_CAR_SUCCESS'

// classification
// -- actions
export const DO_GET_CLASSIFICATION_INFO = 'DO_GET_CLASSIFICATION_INFO'

// -- mutation
export const SET_CLASSIFICATION_INFO = 'SET_CLASSIFICATION_INFO'

// -- actions 商品详情 * 订单详情 * 商品详情分享 * 获取用户身份
export const SHOPDATAILDATA = 'SHOPDATAILDATA'
export const SHOP_DATA_ILGETDATA = 'SHOP_DATA_ILGETDATA'
export const SHOP_SHARE_ILGETDATA = 'SHOP_SHARE_ILGETDATA'
export const SHOP_IDENTITY_ILGETDATA = 'SHOP_IDENTITY_ILGETDATA'

// --mutation 商品详情 * 订单详情 * 商品详情分享 * 获取用户身份
export const SHOPDATAILDATA_MUTATION = 'SHOPDATAILDATA_MUTATION'
export const SHOP_DATA_ILGETDATA_MUTATION = 'SHOP_DATA_ILGETDATA_MUTATION'
export const SHOP_SHARE_ILGETDATA_MUTATION = 'SHOP_SHARE_ILGETDATA_MUTATION'
export const SHOP_IDENTITY_ILGETDATA_MUTATION = 'SHOP_IDENTITY_ILGETDATA_MUTATION'

// getLocalInfo
// -- actions 地址管理
export const SAVEORUPDATE_LOCAL = 'SAVEORUPDATE_LOCAL'
export const DELETE_LOCAL = 'DELETE_LOCAL'
export const FINDALL_LOCAL = 'FINDALL_LOCAL'
export const FINDDEFAULTADDRESS = 'FINDDEFAULTADDRESS'
export const UPDATEDEFAULT = 'UPDATEDEFAULT'

// --mutation
export const SAVEORUPDATE_LOCAL_MUTATION = 'SAVEORUPDATE_LOCAL_MUTATION'
export const DELETE_LOCAL_MUTATION = 'DELETE_LOCAL_MUTATION'
export const FINDALL_LOCAL_MUTATION = 'FINDALL_LOCAL_MUTATION'
export const FINDDEFAULTADDRESS_MUTATION = 'FINDDEFAULTADDRESS_MUTATION'
export const UPDATEDEFAULT_MUTATION = 'UPDATEDEFAULT_MUTATION'

// order
// -- actions
export const CART_TO_ORDER = 'CART_TO_ORDER'
export const GET_ORDER = 'GET_ORDER'
export const SAVE_ORDER = 'SAVE_ORDER'
export const GET_PAY_BACK = 'GET_PAY_BACK'

// -- mutation
export const SET_USER_ACTIONS = 'SET_USER_ACTIONS'
export const SET_ORDER = 'SET_ORDER'
export const SET_SAVE_ORDER = 'SET_SAVE_ORDER'
export const SET_ORDER_EDIT = 'SET_ORDER_EDIT'
export const SET_SAVE_OREDER_URI = 'SET_SAVE_OREDER_URI'
export const CHANGE_SHOP_DATA_ILGETDATA_MUTATION = 'CHANGE_SHOP_DATA_ILGETDATA_MUTATION'

// cache
export const SET_USER_SUCCESS = 'SET_USER_SUCCESS'
export const CHANGE_SKUID = 'CHANGE_SKUID'
export const DO_GET_CACHE_ITEM = 'DO_GET_CACHE_ITEM'
export const SET_CACHE_ITEM = 'SET_CACHE_ITEM'
export const SET_DEL_CACHE_ITEM = 'SET_DEL_CACHE_ITEM'
export const CHANGE_FINDDEFAULTADDRESS_MUTATION = 'CHANGE_FINDDEFAULTADDRESS_MUTATION'

// 获取一二级分类 -- actions
export const GETALLPRIMARYCATEGORYLIST_ACTION = 'GETALLPRIMARYCATEGORYLIST_ACTION'
export const GETALLCATEGORYLIST_ACTION = 'GETALLCATEGORYLIST_ACTION'
export const SHOP_GET_CODE = 'SHOP_GET_CODE'
export const INDEX_CODE_AITIONS = 'INDEX_CODE_AITIONS'

// 获取一二级分类 -- mutation
export const GETALLPRIMARYCATEGORYLIST_MUTAION = 'GETALLPRIMARYCATEGORYLIST_MUTAION'
export const CHANGE_GETALLPRIMARYCATEGORYLIST = 'CHANGE_GETALLPRIMARYCATEGORYLIST'
export const GETALLCATEGORYLIST_MUTATIONS = 'GETALLCATEGORYLIST_MUTATIONS'
export const SHOP_GET_CODE_MUTATION = 'SHOP_GET_CODE_MUTATION'
export const INDEX_CODE_AITIONS_MUTATION = 'INDEX_CODE_AITIONS'
export const SHOP_MES_AGEDATA_SUCCESS = 'SHOP_MES_AGEDATA_SUCCESS'
export const SET_PHONENUMBER = 'SET_PHONENUMBER'

// adolf-modal-end
// action
export const GET_STORELIST_BYLOCATION_ACTION = 'GETSTORELISTBYLOCATIONACTION' // 切换门店
// mutation
export const GET_STORELIST_BYLOCATION_MUTATION = 'GETSTORELISTBYLOCATIONMUTATION'
// adolf-modal-enda
export const SAVE_STORE_ID = 'SAVE_STORE_ID' // 保存门店编号
export const REFRESH_TOKEN = 'REFRESH_TOKEN' // 刷新token
export const SAVE_USER_INFO = 'SAVE_USER_INFO' // 保存用户信息
export const GET_USER_PHONE = 'GET_USER_PHONE' // 获取用户手机号
export const SAVE_USER_PHONE = 'SAVE_USER_PHONE' // 保存用户手机号
export const USER_LOGIN = 'USER_LOGIN' // 用户注册
export const SAVE_SHOP_INFO = 'SAVE_SHOP_INFO' // 保存门店信息
export const GET_SHOP_INFO_BY_ID = 'GET_SHOP_INFO_BY_ID' // 通过 storeId 获取门店信息
export const SET_DEFAULT_STORE = 'SET_DEFAULT_STORE' // 设置门店

export const DO_GET_SHOPDETAIL = 'DO_GET_SHOPDETAIL' // 获取商品详情
export const DO_GET_SHOPCAR_COUNT = 'DO_GET_SHOPCAR_COUNT' // 获取购物车的数量
export const DO_POST_SHOPCARS = 'DO_POST_SHOPCARS' // 添加单个商品到购物车
export const DO_SET_SHOPCAR_COUNT = 'DO_SET_SHOPCAR_COUNT' // 添加单个商品到购物车
export const DO_GET_ADDRESS_DEFAULT = 'DO_GET_ADDRESS_DEFAULT' // 获取默认地址
export const DO_GET_CUSTOMER_DEPOSITS = 'DO_GET_CUSTOMER_DEPOSITS' // 获取红包金额
export const DO_POST_SAVE_ORDER = 'DO_POST_SAVE_ORDER' // 订单支付

export const SET_QUERY_DATA = 'SET_QUERY_DATA' // 保存立即购买的
export const ADDRESS_INFO = 'ADDRESS_INFO'
export const GET_ADDRESS_INFO = 'GET_ADDRESS_INFO'

export const SAVE_CAR_COUNT = 'SAVE_CAR_COUNT'
export const SET_SAVE_CAR_COUNT = 'SET_SAVE_CAR_COUNT'
export const IS_FROM_ORDER_CONFIRM = 'IS_FROM_ORDER_CONFIRM'
export const SAVE_FROM_ADDRESS_ITEM = 'SAVE_FROM_ADDRESS_ITEM'

export const SAVE_ORDER_TABINDEX_COUNT = 'SAVE_ORDER_TABINDEX_COUNT'
export const GET_ORDER_TABINDEX_COUNT = 'GET_ORDER_TABINDEX_COUNT'

export const CART_POINT_POSITION = 'CART_POINT_POSITION'

export const SAVE_NO_PAY_NUM = 'SAVE_NO_PAY_NUM' // 保存未支付数量

export const GET_EXXPRESS_INFO = 'GET_EXXPRESS_INFO'
export const SET_EXXPRESS_INFO = 'SET_EXXPRESS_INFO'
export const SET_TOTAALSRL = 'SET_TOTAALSRL'
export const SHOP_MES_AGEDATA = 'SHOP_MES_AGEDATA' //  获取跳转的code

// 新人有礼的
export const DO_GET_DETAIL_ACTION = 'DO_GET_DETAIL_ACTION' //  获取新人有礼商品详情的额外数据
export const SET_CHECK_SHOP = 'SET_CHECK_SHOP' // 判断门店商品是不是门店商品

// 拼团
export const DO_GET_DETAIL_GROUP = 'DO_GET_DETAIL_GROUP'
export const DO_GET_GIFT_FROM = 'DO_GET_GIFT_FROM'
