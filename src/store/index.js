import Vue from 'vue'
import Vuex from 'vuex'

import state from './state'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'
import my from './modules/my'
import car from './modules/car'
import shopdetail from './modules/shopdetail'
import newManner from './modules/newManner'
import groupBuy from './modules/groupBuy'
import secKill from './modules/secKill'

Vue.use(Vuex)
const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules: {
    my,
    shopdetail,
    car,
    newManner,
    groupBuy,
    secKill
  }
})

export default store
