
const state = {
  openId: '',
  token: '',
  storeId: '', // 门店编号
  login: false, // 是否注册
  userInfo: null, // 用户信息
  shopInfo: {}, // 门店信息
  userPhone: '', // 用户手机号
  baseImgUrl: 'https://dj001.oss-cn-beijing.aliyuncs.com/', // 图片公共访问前缀
  addressInfo: {},
  carCount: 0,
  isFromOrderConfirm: false,
  saveFromAddress: {},
  orderListTabIndex: {},
  cartPointPosition: {},
  noPayNum: {},
  totalSrl: '',
  groupDetail: {}, // 评团的详情
  fromGift: {
    isfromImporove: false
  }
}

export default state
