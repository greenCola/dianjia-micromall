import API from '../api'
import * as types from './types'
import Configure from '@utils/configure'

const actions = {
  [types.GET_OPEN_ID] ({ commit }, query) {
    return API.Public.getOpenId(query).then(res => {
      let data = res.data
      commit(types.GET_OPENID_SUCCESS, data)
      commit(types.REFRESH_TOKEN, data)
      if (data.user) {
        commit(types.SAVE_USER_INFO, data.user)
        commit(types.SAVE_USER_PHONE, data.user.phone)
      }
      return data
    })
  },
  [types.GET_USER_PHONE] ({ commit }, query) {
    return API.Public.getUserPhone(query).then(res => {
      commit(types.SAVE_USER_PHONE, res.data.phoneNumber)
      return res.data
    })
  },
  [types.USER_LOGIN] ({ commit }, query) {
    return API.Public.userLogin(query).then(res => {
      commit(types.REFRESH_TOKEN, res.data)
      return res.data
    })
  },
  [types.SET_DEFAULT_STORE] ({ commit }, query) {
    return API.my.setDefaultStore(query).then(res => {
      commit(types.SAVE_USER_INFO, res.data)
      commit(types.SAVE_USER_PHONE, res.data.phone)
      return res.data
    })
  },
  [types.GET_SHOP_INFO_BY_ID] ({ commit }, {storeId}) {
    if (storeId) {
      return API.Public.getShopInfo({storeId}).then(res => {
        console.log('vuex 中 获得门店信息', res)
        if (res && res.status === 200) {
          let { id, storeName } = res.data
          res.data.storeId = id
          // 设置小程序自定义分析事件 收集门店信息
          Configure.setReportAnalytics('get_store_info', {
            store_id: id,
            store_name: storeName
          })
          commit(types.SAVE_SHOP_INFO, res.data)
          return res.data
        }
      })
    }
  },
  [types.GET_ADDRESS_INFO] ({ commit }, query) {
    return API.my.getCustomerAddressByAddressId(query).then(res => {
      commit(types.ADDRESS_INFO, res.data)
      return res.data
    })
  },
  async [types.SAVE_CAR_COUNT] ({ commit }, query) {
    let { data } = await API.Carts.postShopCarts(query)
    let _data
    if (data) {
      _data = await API.Carts.getCarTotalCount(query)
      if (_data.data) commit(types.SET_SAVE_CAR_COUNT, _data.data)
    }
    return _data.data
  },
  [types.GET_ORDER_TABINDEX_COUNT] ({commit}, query) {
    return API.order.getOrderTabCount(query).then(res => {
      commit(types.SAVE_ORDER_TABINDEX_COUNT, res.data)
      return res.data
    })
  },
  [types.SHOP_MES_AGEDATA] ({ commit }, query) {
    return API.Public.getStoreInfoByStoreCode(query).then(response => {
      console.log(response)
      return response.data
    })
  }
}

export default actions
