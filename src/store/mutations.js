import * as types from './types'
const mutations = {
  [types.LOGIN_OUT] (state) { // 登出
    state.login = false
  },
  [types.GET_OPENID_SUCCESS] (state, data) { // 保存openid 保存门店编号
    state.openId = data.openId
    wx.setStorageSync('openId', data.openId)
    if (data.storeId) {
      this.commit(types.SAVE_STORE_ID, data.storeId)
    }
  },
  [types.REFRESH_TOKEN] (state, data) { // 保存刷新token
    let token = data.token
    if (data.success) {
      if (token) {
        state.login = true
        state.token = token
        wx.setStorageSync('token', token)
      } else {
        state.login = false
      }
    }
  },
  [types.SAVE_STORE_ID] (state, data) { // 保存storeId
    state.storeId = data
    wx.setStorageSync('storeId', data)
  },
  [types.SAVE_USER_INFO] (state, data) { // 保存用户信息
    state.userInfo = data
    wx.setStorageSync('userInfo', data)
  },
  [types.SAVE_USER_PHONE] (state, data) { // 保存用户手机号
    state.userPhone = data
    wx.setStorageSync('userPhone', data)
  },
  [types.SAVE_SHOP_INFO] (state, data = {}) { // 保存门店信息
    state.shopInfo = data
    this.commit(types.SAVE_STORE_ID, data.storeId)
  },
  [types.ADDRESS_INFO] (state, data) {
    state.addressInfo = data
  },
  [types.SET_SAVE_CAR_COUNT] (state, data) {
    state.carCount = data.count
  },
  [types.IS_FROM_ORDER_CONFIRM] (state, data) {
    state.isFromOrderConfirm = data
  },
  [types.SAVE_FROM_ADDRESS_ITEM] (state, data) {
    state.saveFromAddress = data
  },
  [types.SAVE_ORDER_TABINDEX_COUNT] (state, data) {
    state.orderListTabIndex = data
  },
  [types.CART_POINT_POSITION] (state, data) {
    state.cartPointPosition = data
  },
  [types.SAVE_NO_PAY_NUM] (state, data) {
    state.noPayNum = data
  },
  [types.SET_TOTAALSRL] (state, data) {
    state.totalSrl = data
  },
  // 拼团的详情
  [types.DO_GET_DETAIL_GROUP] (state, data) {
    state.groupDetail = data
  },
  [types.DO_GET_GIFT_FROM] (state, data) {
    state.fromGift = data
  }
}

export default mutations
