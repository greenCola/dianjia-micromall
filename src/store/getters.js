
const getters = {
  openId: state => state.openId,
  token: state => state.token,
  storeId: state => state.storeId,
  login: state => state.login,
  userInfo: state => state.userInfo,
  shopInfo: state => state.shopInfo,
  userPhone: state => state.userPhone,
  baseImgUrl: state => state.baseImgUrl,
  addressInfo: state => state.addressInfo,
  carCount: state => state.carCount,
  isFromOrderConfirm: state => state.isFromOrderConfirm,
  saveFromAddress: state => state.saveFromAddress,
  orderListTabIndex: state => state.orderListTabIndex,
  cartPointPosition: state => state.cartPointPosition,
  noPayNum: state => state.noPayNum,
  totalSrl: state => state.totalSrl,
  groupDetail: state => state.groupDetail,
  fromGift: state => state.fromGift
}

export default getters
