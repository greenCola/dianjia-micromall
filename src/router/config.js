module.exports = {
  Order: {
    OrderList: '/pages/Order/list', // 我的订单  '/pages/order/orderList'
    OrderDetail: '/pages/Order/detail', // 订单详情 '/pages/orderDetailInfo/index'
    OrderConfirm: '/pages/Order/confirm', // 确认订单 '/pages/orderDEtail/index'
    OrderActivityConfirm: '/pages/Order/activityConfirm', // 活动确认订单 礼物 拼团
    OrderResutl: '/pages/Order/resutl' // 支付结果 '/pages/success/index'
  },
  Goods: {
    GoodsList: '/pages/Goods/list', // 商品列表  '/pages/Goods/list'
    GoodsSearchList: '/pages/Goods/searchList', // 商品搜索 '/pages/Goods/searchList'
    GoodsDetail: '/pages/Goods/detail', // 商品详情 '/pages/commodityDetails/index'
    GoodsActivityDetail: '/pages/Goods/activitysDetail' // 活动商品详情
  },
  Address: {
    List: '/pages/Address/manageAddress', // 管理地址 /pages/manageAddress/index
    Add: '/pages/Address/addAddress', // 管理地址
    Edit: '/pages/Address/editAddress', // 管理地址
    ChangeStore: '/pages/Address/changeStore' // 切换门店
  },
  Activitys: {
    NewManner: '/pages/Activitys/newManner', // 新人礼
    RedPacket: '/pages/Activitys/redPacketDetails', // 领取红包
    Notice: '/pages/Activitys/notice' // 公告模版消息
  },
  Auth: {
    Start: '/pages/Auth/start', // 中转页面 判断跳转 'pages/bootPage/index'
    Login: '/pages/Auth/login', // 授权登录  'pages/impower/index'
    Loading: '/pages/loading/index' // 物料码loading
  },
  Common: {
    HelpCenter: '/pages/Common/helpCenter', // 帮助中心
    Logistics: '/pages/Common/logistics' // 物流详情
  },
  GroupBuy: {
    List: '/pages/GroupBuy/list', // 拼团活动
    ActivityDetail: '/pages/GroupBuy/activityDetail', // 活动详情
    Detail: '/pages/GroupBuy/detail', // 拼团详情
    MyGroupBuyList: '/pages/GroupBuy/myGroupBuyList' // 我的拼团
  },
  Gift: {
    List: '/pages/SendGift/list', // 送礼物活动
    ActivityDetail: '/pages/SendGift/activityDetail', // 活动详情
    Index: '/pages/SendGift/index', // 送礼物详情
    GiftResult: '/pages/SendGift/giftResult', // 礼物领取页面
    OperateInfo: '/pages/SendGift/operateInfo' // 送礼物操作指南
    // MyGiftList: '/pages/SendGift/myGiftList', // 我的送礼物
  },
  SecKill: {
    List: '/pages/SecKill/list', // 秒杀列表
    ActivityDetail: '/pages/SecKill/activityDetail', // 详情
    OrderConfirm: '/pages/SecKill/confrim', // 秒杀订单
    MySecKillList: '/pages/SecKill/myList' // 我的秒杀
  },
  Store: {
    List: '/pages/Store/storeList', // 门店列表
    search: '/pages/Store/storeSearch' // 门店搜索
  },
  Coupon: {
    list: '/pages/coupon/list', // 我的优惠券
    MyList: '/pages/coupon/myList' // 我的优惠券商城
  },
  PayLine: {
    pay: '/pages/PayLine/pay' // 线下支付
  },
  Home: '/pages/Home/index',
  Carts: '/pages/Carts/index' // 购物车
}
