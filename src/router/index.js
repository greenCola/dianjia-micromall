const RouterPath = require('./config')
module.exports = [
  // 启动页
  {
    path: RouterPath.Auth.Start,
    config: {
      navigationBarTitleText: '',
      backgroundColorTop: '#ccc',
      disableScroll: true
    }
  },
  // 授权登录页
  {
    path: RouterPath.Auth.Login,
    config: {
      navigationBarTitleText: '授权登录',
      disableScroll: true
    }
  },
  // home 页
  {
    path: RouterPath.Home,
    config: {
      // disableScroll: true,
      enablePullDownRefresh: true
    }
  },
  // 我的订单
  {
    path: RouterPath.Order.OrderList,
    config: {
      navigationBarTitleText: '我的订单'
    }
  },
  // 商品列表
  {
    path: RouterPath.Goods.GoodsList,
    config: {
      navigationBarTitleText: '商品列表',
      onReachBottomDistance: 50
    }
  },
  // 商品搜索列表
  {
    path: RouterPath.Goods.GoodsSearchList,
    config: {
      navigationBarTitleText: '搜索',
      onReachBottomDistance: 50
    }
  },
  // 商品详情
  {
    path: RouterPath.Goods.GoodsDetail,
    config: {
      navigationBarTitleText: '商品详情'
    }
  },
  // 活动商品详情
  {
    path: RouterPath.Goods.GoodsActivityDetail,
    config: {
      navigationBarTitleText: '商品详情'
    }
  },
  // 订单详情
  {
    path: RouterPath.Order.OrderDetail,
    config: {
      navigationBarTitleText: '订单详情'
    }
  },
  // 确认订单
  {
    path: RouterPath.Order.OrderConfirm,
    config: {
      navigationBarTitleText: '确认订单'
    }
  },
  // 拼团-礼物说-确认订单
  {
    path: RouterPath.Order.OrderActivityConfirm,
    config: {
      navigationBarTitleText: '确认订单'
    }
  },
  // 支付结果
  {
    path: RouterPath.Order.OrderResutl,
    config: {
      navigationBarTitleText: '支付结果'
    }
  },
  // 商品搜索 商品列表
  {
    path: RouterPath.Address.ChangeStore,
    navigationBarTitleText: '切换门店'
  },
  // 管理地址
  {
    path: RouterPath.Address.List,
    config: {
      navigationBarTitleText: '管理地址'
    }
  },
  // 增加地址
  {
    path: RouterPath.Address.Add,
    config: {
      navigationBarTitleText: '管理地址'
    }
  },
  // 查看物流
  {
    path: RouterPath.Common.Logistics,
    config: {
      navigationBarTitleText: '查看物流'
    }
  },
  // 编辑地址
  {
    path: RouterPath.Address.Edit,
    config: {
      navigationBarTitleText: '管理地址'
    }
  },
  // 红包页面
  {
    path: RouterPath.Activitys.RedPacket,
    config: {
      navigationBarTitleText: '领取红包',
      backgroundColorTop: '#ccc'
    }
  },
  // 新人有礼
  {
    path: RouterPath.Activitys.NewManner,
    config: {
      navigationBarTitleText: '新人有礼',
      backgroundColorTop: '#ccc'
    }
  },
  // 公告页面
  {
    path: RouterPath.Activitys.Notice,
    config: {
      navigationBarTitleText: '门店公告',
      backgroundColorTop: '#ccc'
    }
  },
  // 购物车
  {
    path: RouterPath.Carts,
    config: {
      navigationBarTitleText: '购物车'
    }
  },
  // 帮助中心
  {
    path: RouterPath.Common.HelpCenter,
    config: {
      navigationBarTitleText: '帮助中心'
    }
  },
  // 物料码loading
  {
    path: RouterPath.Auth.Loading,
    config: {
      navigationBarTitleText: '母婴24',
      backgroundColorTop: '#ccc'
    }
  },
  // 拼团活动
  {
    path: RouterPath.GroupBuy.List,
    config: {
      navigationBarTitleText: '拼团',
      backgroundColorTop: '#ccc'
    }
  },
  // // 拼团 活动详情
  {
    path: RouterPath.GroupBuy.ActivityDetail,
    config: {
      navigationBarTitleText: '拼团',
      backgroundColorTop: '#ccc',
      onReachBottomDistance: 50
    }
  },
  // 拼团 拼团详情
  {
    path: RouterPath.GroupBuy.Detail,
    config: {
      navigationBarTitleText: '',
      backgroundColorTop: '#ccc'
    }
  },
  // 拼团 我的拼团活动
  {
    path: RouterPath.GroupBuy.MyGroupBuyList,
    config: {
      navigationBarTitleText: '我的拼团',
      backgroundColorTop: '#ccc',
      onReachBottomDistance: 50
    }
  },
  // 送礼 列表
  {
    path: RouterPath.Gift.List,
    config: {
      navigationBarTitleText: '送礼物',
      backgroundColorTop: '#ccc'
    }
  },
  // 礼物 活动详情
  {
    path: RouterPath.Gift.ActivityDetail,
    config: {
      navigationBarTitleText: '礼物',
      backgroundColorTop: '#cb1f2d',
      onReachBottomDistance: 50
    }
  },
  // 礼物 送礼物详情
  {
    path: RouterPath.Gift.Index,
    config: {
      navigationBarTitleText: '送礼物',
      backgroundColorTop: '#cb1f2d'
    }
  },
  // // 礼物 我的礼物活动
  // {
  //   path: RouterPath.Gift.MyGiftList,
  //   config: {
  //     navigationBarTitleText: '我的礼物',
  //     backgroundColorTop: '#ccc',
  //     onReachBottomDistance: 50
  //   }
  // },
  // 礼物 礼物领取页面
  {
    path: RouterPath.Gift.GiftResult,
    config: {
      navigationBarTitleText: '送礼物',
      backgroundColorTop: '#B91120'
    }
  },
  // 礼物 礼物领取页面
  {
    path: RouterPath.Gift.OperateInfo,
    config: {
      navigationBarTitleText: '送礼物',
      backgroundColorTop: '#ccc'
    }
  },
  // 门店搜索
  {
    path: RouterPath.Store.search,
    config: {
      navigationBarTitleText: '找门店',
      backgroundColorTop: '#ccc'
    }
  },
  // 门店列表
  {
    path: RouterPath.Store.List,
    config: {
      navigationBarTitleText: '找门店',
      backgroundColorTop: '#ccc'
    }
  },
  // 秒杀--模块
  {
    path: RouterPath.SecKill.List,
    config: {
      navigationBarTitleText: '秒杀',
      backgroundColorTop: '#ccc'
    }
  },
  // 详情
  {
    path: RouterPath.SecKill.ActivityDetail,
    config: {
      navigationBarTitleText: '商品详情',
      backgroundColorTop: '#ccc'
    }
  },
  // 订单详情
  {
    path: RouterPath.SecKill.OrderConfirm,
    config: {
      navigationBarTitleText: '订单详情',
      backgroundColorTop: '#ccc'
    }
  },
  // 我的秒杀
  {
    path: RouterPath.SecKill.MySecKillList,
    config: {
      navigationBarTitleText: '我的秒杀',
      backgroundColorTop: '#ccc'
    }
  },
  // 我的优惠券
  {
    path: RouterPath.Coupon.list,
    config: {
      navigationBarTitleText: '优惠券',
      backgroundColorTop: '#ccc'
    }
  },
  // 我的优惠券商城
  {
    path: RouterPath.Coupon.MyList,
    config: {
      navigationBarTitleText: '优惠券',
      backgroundColorTop: '#ccc'
    }
  },
  // 线下支付
  {
    path: RouterPath.PayLine.pay,
    config: {
      navigationBarTitleText: '订单支付',
      backgroundColorTop: '#ccc'
    }
  }
]
