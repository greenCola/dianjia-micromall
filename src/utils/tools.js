class Tools {
  static showLoading (text) { // 显示加载
    if (wx && wx.showLoading) {
      wx.showLoading({
        title: String(text),
        mask: true
      })
    } else {
      wx.showToast({
        title: String(text),
        icon: 'loading',
        mask: true
      })
    }
  }
  static hideLoading () { // 显示加载
    if (wx && wx.hideLoading) {
      wx.hideLoading()
    } else {
      wx.hideToast()
    }
  }
  static toast (text = '', icon = 'none', duration = 3000) { // 提示
    wx.showToast({
      title: String(text),
      icon,
      duration
    })
  }
  static deepCopy (p, c) { // 对象拷贝
    c = (c || {})
    for (var i in p) {
      if (p[i] && typeof p[i] === 'object') {
        c[i] = (p[i].constructor === Array) ? [] : {}
        this.deepCopy(p[i], c[i])
      } else {
        c[i] = p[i]
      }
    }
    return c
  }
  static setUrlObj (obj) { // 对象转url传参
    let str = []
    for (let i in obj) {
      str.push(i + '=' + this.setFilterMark(obj[i]))
    }
    return str.join('&')
  }
  static setFilterMark (obj) { // 过滤url 特殊符号      &=1 ?=2
    return (JSON.stringify(obj)).replace(new RegExp(/(&)/g), 'mark-1').replace(new RegExp(/(\?)/g), 'mark-2')
  }
}
export default Tools
