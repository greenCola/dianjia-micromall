import Api from '@api'
const RouterPath = require('../router/config.js')
const Routers = require('../router/index.js')
export default class Configure {
  /**
   * @description 设置设备信息
   */
  static setSystemInfo () {
    if (wx.getSystemInfo) {
      wx.getSystemInfo({
        success: res => {
          // console.log(res)
          global.ww = res.windowWidth
          global.hh = res.windowHeight
          global.system = res.system
        }
      })
    }
  }
  /**
   * @description 强制更新
   */
  static forceUpdate () {
    // wx.getUpdateManager 在 1.9.90 才可用，请注意兼容
    if (wx.getUpdateManager) {
      const updateManager = wx.getUpdateManager()
      updateManager.onCheckForUpdate(function (res) {
        // 请求完新版本信息的回调
        console.log('检测是否有新的版本：', res.hasUpdate)
      })
      updateManager.onUpdateReady(function () {
        wx.showModal({
          title: '更新提示',
          content: '新版本已经准备好，是否马上重启小程序？',
          showCancel: false,
          success: function (res) {
            if (res.confirm) {
              // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
              updateManager.applyUpdate()
            }
          }
        })
      })
      updateManager.onUpdateFailed(function () {
        // 新的版本下载失败
      })
    }
  }

  /**
   * @description 默认路由
   */
  static defaultRouter (opt) {
    console.log(opt.path, 'opt.path--小程序进入后的第一个路径')

    let _path = opt.path.indexOf('/') ? '/' + opt.path : opt.path
    let _allowrRouter = []

    Routers.map(p => {
      _allowrRouter.push(p.path)
    })
    if (_allowrRouter.indexOf(_path) === -1) {
      console.warn('找不到' + _path + '页面', _allowrRouter)
      wx.redirectTo({
        url: RouterPath.Auth.Start
      })
    }
  }
  /**
   * @description 设置小程序自定义分析事件
   */
  static setReportAnalytics ({name = '', query = {}}) {
    if (name) {
      wx.reportAnalytics(name, query)
    }
  }

  /**
   * @description 发送模版消息
   * params
   *  @param {String} totalSrl【必填(支付成功回掉】订单号
   *  @param {String} formId【必填(支付成功回掉】formId 微信模版的from表单的中值
   *  @param {String} sessionId【必填(支付成功回掉)】prepay_id
   */
  static async sendWxMessage (params) {
    console.log(params, '这是数据请求的内容')
    let _res = await Api.Public.sendWxMessage(params)
    if (_res && _res.status === 200) {
      console.log(_res.data, '这是一个模版消息哈哈哈哈')
      return _res.data
    }
  }
  /**
   * @description 预约提醒
   * params
   *  @param {String} formId【必填(支付成功回掉】formId 微信模版的from表单的中值
   * @param {String} id【必填(支付成功回掉】id 活动的id
   */
  static async postSubscric (params) {
    console.log(params, '这是数据请求的内容')
    let _res = await Api.SecKill.postSubscribes(params)
    if (_res && _res.status === 200) {
      console.log(_res.data, '预约提醒')
      return _res.data
    }
  }
}
