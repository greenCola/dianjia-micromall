/*
 * @Title: 贝塞尔曲线绘制
 * @Author: 沈童童
 * @Date: 2018-11-12 19:06:46
 * @LastEditTime: 2018-11-18 11:14:49
 */
/**
 * @param  {Object} targetPositionXY 终点目标位置
 * @return {String} pointsAmount 绘制的点的个数如：30
 * @return {obj} e 点击时event事件
 */
class Bezier {
  constructor (targetPositionXY, pointsAmount, e) {
    this.targetPositionXY = targetPositionXY
    this.pointsAmount = pointsAmount
    this.finger = {}
    this._event = e
    this.hide_good_box = true
    this.bus_x = 0
    this.bus_y = 0
    // 贝塞尔曲线"点"的绘制
    this.draw = function (anchorpoints) {
      var points = []
      for (var i = 0; i < this.pointsAmount; i++) {
        var point = this.MultiPointBezier(anchorpoints, i / this.pointsAmount)
        points.push(point)
      }
      return points
    }
    // 计算Bezier点
    /**
     * @param  {Array} points [目标点，移动的最高点，起始点]
     */
    this.MultiPointBezier = function (points, t) {
      var len = points.length
      var x = 0
      var y = 0
      var erxiangshi = function (start, end) {
        var cs = 1
        var bcs = 1
        while (end > 0) {
          cs *= start
          bcs *= end
          start--
          end--
        }
        return cs / bcs
      }
      for (var i = 0; i < len; i++) {
        var point = points[i]
        x +=
        point.x *
        Math.pow(1 - t, len - 1 - i) *
        Math.pow(t, i) *
        erxiangshi(len - 1, i)
        y +=
        point.y *
        Math.pow(1 - t, len - 1 - i) *
        Math.pow(t, i) *
        erxiangshi(len - 1, i)
      }
      return { x: x, y: y }
    }
  }
  clickOnGoods (cb) {
    var topPoint = {}
    this.finger['x'] = this._event.touches['0'].clientX // 点击的位置
    this.finger['y'] = this._event.touches['0'].clientY
    if (this.finger['y'] < this.targetPositionXY['y']) {
      topPoint['y'] = this.finger['y'] - 150
    } else {
      topPoint['y'] = this.targetPositionXY['y'] - 150
    }
    topPoint['x'] =
      Math.abs(this.finger['x'] - this.targetPositionXY['x']) / 2

    if (this.finger['x'] > this.targetPositionXY['x']) {
      topPoint['x'] =
        (this.finger['x'] - this.targetPositionXY['x']) / 2 +
        this.targetPositionXY['x']
    } else {
      topPoint['x'] =
        (this.targetPositionXY['x'] - this.finger['x']) / 2 +
        this.finger['x']
    }
    this.linePos = this.draw(
      [this.targetPositionXY, topPoint, this.finger],
      30
    )
    this.startAnimation(cb)
  }
  startAnimation (cb) {
    var index = 0
    var that = this
    var bezierPoints = that.linePos.reverse()
    this.hide_good_box = false
    this.bus_x = that.finger['x'] - 10
    this.bus_y = that.finger['y'] - 10
    var len = bezierPoints.length
    index = len
    for (let i = index - 1; i > -1; i--) {
      (function (e) {
        that.timer = setTimeout(() => {
          that.bus_x = bezierPoints[e]['x']
          that.bus_y = bezierPoints[e]['y']
          if (e >= 29) {
            that.hide_good_box = true
            cb()
          }
        }, 25 * e)
      })(i)
    }
  }
}

export default Bezier
