export default {
  onEvent: function (title, labels, param) {
    let app = getApp()
    app.td_app_sdk.event({
      id: title,
      label: labels,
      params: param
    })
  }
}
