export default {
  // DEFAUTL_URL: 'https://img.dianjia001.com/online/opmactivitymg/1f198f388cd647e1a8e41852e81bc960.png?', // 默认的图片路径
  DEFAUTL_URL: 'https://dj001.oss-cn-beijing.aliyuncs.com/online/opmactivitymg/87d9cfd030b84e8a9f888a0adf0808b2.png?', // 默认的图片路径
  DEFAULT_SHOP_DATAILS_URL: 'https://img.dianjia001.com/online/opmactivitymg/630f7e0d6a2e47fd9e1cac268e13f51d.png', // 默认的图片路径
  BASE_IMG_URL: 'https://img.dianjia001.com/', // 图片公共访问前缀
  banner: [
    // 1: 正常商品 2: 活动 0： 活动商品 3：是无跳转
    // { // 聪妈新年促销惠享款抽取式纸巾    4层/300张/30包
    //   id: '6162550',
    //   type: '1',
    //   url: 'https://img.dianjia001.com/online/opmactivitymg/fa2fb1c23cf741e6880e21d99932b52c.jpeg?x-oss-process=image/resize,m_lfit,w_670/sharpen,80'
    // },
    { // newManner-banner
      id: '',
      type: '3',
      url: 'http://dj001.oss-cn-beijing.aliyuncs.com/online/opmactivitymg/00f1bfaeb41d4c478ddb399f733f2b37.png'
    }
    // { // newManner-banner
    //   id: '',
    //   type: '3',
    //   url: 'https://dj001.oss-cn-beijing.aliyuncs.com/online/opmactivitymg/8657d3e15e1b4f46b37e4740936e912f.jpeg'
    // }
    // { // newManner-banner
    //   id: '',
    //   type: '2',
    //   url: 'https://dj001.oss-cn-beijing.aliyuncs.com/online/opmactivitymg/f7ef623b43ac477f8c097204b18ca038.png?x-oss-process=image/resize,m_lfit,w_670/sharpen,80'
    // }
    // { // newManner-banner
    //   id: '',
    //   type: '2',
    //   http://dj001.oss-cn-beijing.aliyuncs.com/online/opmactivitymg/f7ef623b43ac477f8c097204b18ca038.png
    //   url: 'https://img.dianjia001.com/online/opmactivitymg/ad17d3c856c44945bb8a77af0ab9140a.png?x-oss-process=image/resize,m_lfit,w_670/sharpen,80'
    // }
    // { // 贝得宝可爱儿童牙刷牙膏套装    50g
    //   id: '4642296',
    //   type: '1', // 正常商品
    //   url: 'https://img.dianjia001.com/online/opmactivitymg/bcf2899044df4244b31709f10a5e0cad.jpg?x-oss-process=image/resize,m_lfit,w_670/sharpen,80'
    // },
    // { // oright保温碗五件套（含叉子，勺子） 红色
    //   id: '4820520',
    //   type: '1', // 正常商品
    //   url: 'https://img.dianjia001.com/online/opmactivitymg/c2d3d1b9640d44e59930134b20ab18e4.jpg?x-oss-process=image/resize,m_lfit,w_670/sharpen,80'
    // }
  ],
  RESULT_ORDER: 'https://img.dianjia001.com/online/opmactivitymg/5f5c3163fc494285b8d7bd50031328c5.png?x-oss-process=image/resize,m_lfit,w_670/sharpen,80',
  REDPACK_BANNER: 'https://img.dianjia001.com/online/opmactivitymg/d929d87c7f3145b09145103f15e5460b.png?x-oss-process=image/resize,m_lfit,w_670/sharpen,80',
  COUPON: 'http://dj001.oss-cn-beijing.aliyuncs.com/online/opmactivitymg/ec935e9c209745fab76f3038d18af565.png?x-oss-process=image/resize,m_lfit,w_670/sharpen,80',
  OVERCOUPON: 'http://dj001.oss-cn-beijing.aliyuncs.com/online/opmactivitymg/c4ce1e5f9e3c439091c5b9ccf330d750.png?x-oss-process=image/resize,m_lfit,w_670/sharpen,80',
  COUPONRED: 'http://dj001.oss-cn-beijing.aliyuncs.com/online/opmactivitymg/34bca58be60e41bba0a003ffd0d10db6.png?x-oss-process=image/resize,m_lfit,w_670/sharpen,80'
}
