import Contants from './index'
export const HeadImgList = [
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/17cf51b15ac149b4a9089fba387f1f5e.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/0c23a8f489b747909f71b00484539bcd.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/cc317655de944ade9920d77a017fd146.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/48cd3d0a245a4089870eab87894aa4e1.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/3c6b565ec01a407da73cdd586c9f2dbf.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/b75d1de401ee4af8a12ac18bf00b0c1a.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/82a20b62e0074003843950948a7e779e.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/70981ab1443c41228b1f90f17730f6eb.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/71404065671e438ebf235da45e0edc12.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/fea564ac9d2f4d2b94c56299b6cd756c.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/ac4bccedc5524e79bd63be09bb297b1a.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/31403ddf751144e7883296785d3a9b42.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/35b7376da1374734a7662b6977647dee.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/2c84884de04440cdbea833e166f94671.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/f8c00adadc644d00a5a1bf2248ed23ac.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/5d0290a18376418abdd9da2a3a59d7e5.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/cc1d099ec23b422a94424d0882bc9cb7.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/87749ccd3a8b43b29eb190499f198c17.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/22040d6819cc45788ba2b2145e5dedb6.png`},
  {headImg: `${Contants.BASE_IMG_URL}online/opmactivitymg/7cca33c92a0842729040451d00b8024f.png`}
]
// export const GROU_BUY_BANNER = `${Contants.BASE_IMG_URL}online/opmactivitymg/f8e05f55b42b40c7bb39d5b6a8cf89db.jpg?x-oss-process=image/resize,m_lfit,w_670/sharpen,80`
export const GROU_BUY_BANNER = `${Contants.BASE_IMG_URL}online/opmactivitymg/21a19d0aa01e4c0e9f509f2a41638a1c.png?x-oss-process=image/resize,m_lfit,w_670/sharpen,80`

export const NO_CONTENT = `${Contants.BASE_IMG_URL}online/opmactivitymg/b4dc4e313acb4161aac4861d85a8058e.jpg?x-oss-process=image/resize,m_lfit,w_210/sharpen,80`
