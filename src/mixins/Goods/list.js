import RouterPath from '@router/config'
import * as types from '@store/types'
import Tools from '@utils/tools'
import Utils from '@utils/utils'
import API from '@api'
import { mapGetters } from 'vuex'

export default{
  data () {
    return {
      keyword: '',
      goodsList: [],
      loadPage: false,
      pageIndex: 0,
      isloadMoreGoods: false,
      showPageBootom: false,
      text: '现在还没有该商品哦～'
    }
  },
  computed: {
    ...mapGetters(['login', 'storeId'])
  },
  methods: {
    // 正常商品获取数据
    searchGoodsReq (obj, type) {
      API.Search.searchGoods(obj)
        .then(res => {
          wx.hideLoading()
          if (res.status === 200) {
            let data = Utils.resolveData(res.data.content, [{ path: 'skuImg', width: 200 }])
            this.loadPage = true
            this.goodsList = type ? [...this.goodsList, ...data] : [...data]
            // console.log(data, 'this.data')
            // console.log(this.goodsList, 'this.goodsLis')
            if (data.length) {
              this.isloadMoreGoods = false
              this.pageIndex = obj.page
            } else if (data.length === 0 && type) {
              this.showPageBootom = true
            }
          }
        })
        .catch(rest => {
          this.loadPage = true
          this.isloadMoreGoods = false
        })
    },
    // 继续加载商品请求
    loadMoreGoods () {
      console.log('加载更多')
      if (!this.isloadMoreGoods) {
        this.isloadMoreGoods = true
        this.getGoodsList(true)
      }
    },
    // 获取分页数据
    getPageData () {
      this.pageIndex = 0
      this.showPageBootom = false
      this.getGoodsList()
      Tools.hideLoading()
    },
    // 添加购物车
    addCar (item) {
      const sendObject = {
        storeSkuId: item.id,
        storeId: this.storeId,
        count: 1
      }
      console.log(this.login, 'this.login')
      if (this.login) {
        this.$store.dispatch(types.SAVE_CAR_COUNT, sendObject).then(res => {
          wx.showToast({
            title: '已添加购物车',
            icon: 'success',
            duration: 1000
          })
          this.$emit('changeCount', res.count)
        })
      } else {
        wx.navigateTo({
          url: RouterPath.Auth.Login + '?fromPage=index'
        })
      }
    }
  }
}
