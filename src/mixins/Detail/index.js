import RouterPath from '@router/config'
// import * as types from '@store/types'
import Contants from '@contants/index'

import DTotast from '@components/UI/DTotast'

/*
 * @Title: 详情页面公共方法混入
 * @Author: txy
 * @Date: 2018-11-12 19:06:46
 * @LastEditTime: 2018-12-18 11:20:42
 * @LastEditors: Please set LastEditors
 * @Description:  涉及的页面和组件有：</pages/Goods/detail,/pages/Goods/activitysDetail,/pages/components/Order/ProjectileFrame.vue>
 */
export default{
  data () {
    return {
      Contants,
      num: 1 // 限购的数量
    }
  },
  components: {
    DTotast
  },
  methods: {
    // 返回首页的
    goHomeBtn_clickHander () {
      wx.reLaunch({
        url: RouterPath.Home
      })
    },
    // 点击减号
    minus_clickHander () {
      if (this.num === 1) {
        return false
      } else {
        this.num--
      }
    },
    // 点击加号
    plus (limitCount) {
      if (limitCount && limitCount <= this.num) {
        this.$refs.totast.showToast({title: '该商品只能购买' + limitCount + '件~'})
      } else {
        this.num++
      }
      console.log(this.num)
    },
    // 分享
    shareMessage_handler (title, path = Contants.DEFAUTL_URL, imageUrl) {
      return {
        title,
        path,
        imageUrl
      }
    }

  }
}
