import { mapGetters } from 'vuex'
export default{
  data () {
    return {
      test: '这是测试mixins'
    }
  },
  computed: {
    ...mapGetters(['shopInfo', 'userInfo']),
    //  判断是不是新用户
    isNewUser () {
      let isNewUser = true
      if ((this.shopInfo && this.shopInfo.id && this.shopInfo.userId) && (this.userInfo && this.userInfo.id)) {
        isNewUser = false
      }
      console.log(isNewUser, '是不是新用户')
      return isNewUser
    }
  }
}
